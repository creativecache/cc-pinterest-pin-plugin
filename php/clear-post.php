<?php

$filename = $_POST['titleslug'];
$successMessage = 'Image has been successfully cleared!';

//The name of the folder.
$upload_dir = wp_upload_dir(); 
$folder = $upload_dir['basedir'] . '/pinterest/';
 
//Get a list of all of the file names in the folder.
$file = $folder . $filename . '.png';
 
if(is_file($file)){
	unlink($file);
}

echo $successMessage;

?>