<?php

    //defined( 'ABSPATH' ) or die( 'No script please!' );

	global $post;
	$post_slug = $post->post_name;
	$uploads = wp_upload_dir();
	$upload_path = $uploads['baseurl'];

	global $wpdb;
	$image_src = wp_upload_dir()['basedir'] . '/pinterest/' . _wp_relative_upload_path( $post_slug . '.png' );
?>

<?php if ( has_post_thumbnail() && is_single() ) : ?>
	<?php if (!file_exists($image_src)) : ?>
	<div id="mmsppg-capture" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
		<img src="<?php the_post_thumbnail_url(); ?>" class="pin-image-background"/>
		<div class="content">
			<h1 class="title"><?php the_title(); ?></h1>
			<?php
			$permalink = get_site_url();
			$find = array( 'http://', 'https://' );
			$replace = '';
			$output = str_replace( $find, $replace, $permalink );
			?>
			<span class="blog-url"><?php echo $output ?></span>
		</div>
	</div>
	<?php endif; ?>
	
	<div id="mmsppg-pinterest-image-generator" class="widget widget_pinterest_image<?php if (file_exists($image_src)) : ?> visible<?php endif; ?>">
		
		<?php if (!file_exists($image_src)) : ?>
		
		<a target="_blank" data-slug="<?php echo $post_slug; ?>" href="http://pinterest.com/pin/create/link/?url=<?php echo urlencode( get_permalink() ); ?>&description=<?php echo urlencode( get_the_title() ); ?>&media=" id="mmsppg-pinterest-image"><i class="fab fa-pinterest"></i><img src="<?php echo $upload_path ?>/pinterest/" alt="<?php the_title(); ?>" data-template-uri="<?php echo get_template_directory_uri(); ?>" /></a>
		
		<?php else :?>
		
		<a target="_blank" href="http://pinterest.com/pin/create/link/?url=<?php echo urlencode( get_permalink() ); ?>&description=<?php echo urlencode( get_the_title() ); ?>&media=<?php echo $upload_path ?>/pinterest/<?php echo $post_slug; ?>.png" id="mmsppg-pinterest-image"><i class="fab fa-pinterest"></i><img src="<?php echo $upload_path ?>/pinterest/<?php echo $post_slug; ?>.png" alt="<?php the_title(); ?>" /></a>
		
		<?php endif;?>
		
	</div>
<?php endif; ?>